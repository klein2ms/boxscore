package boxscore

import (
	"testing"
)

func TestScore(t *testing.T) {
	obj := Objective{KeyResults: []KeyResult{}}
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: true})
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: true})
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: true})
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: false})

	const expect = 0.75
	got, _ := obj.CurrentScore()

	if got != expect {
		t.Fatalf("expected %f; got %f", expect, got)
	}
}

func TestIsAchieved(t *testing.T) {
	obj := Objective{KeyResults: []KeyResult{}, CompletionThreshold: 0.6}
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: true})
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: true})
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: true})
	obj.KeyResults = append(obj.KeyResults, KeyResult{IsComplete: false})

	const expect = true
	got, _ := obj.IsAchieved()

	if got != expect {
		t.Fatalf("expected %t; got %t", expect, got)
	}
}
