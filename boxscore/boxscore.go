// Package boxscore provides domain objects that describe
// the OKR framework and its application
package boxscore

import (
	"time"
)

type Cycle struct {
	Id                  string      `json:"id,omitempty"`
	Start               time.Time   `json:"start,omitempty"`
	End                 time.Time   `json:"end,omitempty"`
	Objectives          []Objective `json:"objectives,omitempty"`
	CompletionThreshold float32     `json:"completion_threshold,omitempty"`
}

func (c Cycle) IsAchieved() (bool, error) {
	return isAchieved(c.achievables(), c.CompletionThreshold)
}

func (c Cycle) Score() (float32, error) {
	return score(c.achievables())
}

func (c Cycle) achievables() []achiever {
	achievables := make([]achiever, len(c.Objectives))
	for i, v := range c.Objectives {
		achievables[i] = v
	}
	return achievables
}

type ObjectiveType int

const (
	Unassigned ObjectiveType = iota
	Personal
	Team
)

func (ot ObjectiveType) String() string {
	return [...]string{"Unassigned", "Personal", "Team"}[ot]
}

type ConfidenceLevel int

const (
	UnknownConfidenceLevel ConfidenceLevel = iota
	Red
	Yellow
	Green
)

func (cl ConfidenceLevel) String() string {
	return [...]string{"Unknown", "Red", "Yellow", "Green"}[cl]
}

type PrivacyLevel int

const (
	Public PrivacyLevel = iota
	Private
)

func (pl PrivacyLevel) String() string {
	return [...]string{"Public", "Private"}[pl]
}

type Objective struct {
	ID                  string          `json:"id,omitempty"`
	Title               string          `json:"title,omitempty"`
	Description         string          `json:"description,omitempty"`
	KeyResults          []KeyResult     `json:"key_results,omitempty"`
	CompletionThreshold float32         `json:"completion_threshold,omitempty"`
	Type                ObjectiveType   `json:"type,omitempty"`
	Confidence          ConfidenceLevel `json:"confidence,omitempty"`
	Privacy             PrivacyLevel    `json:"privacy,omitempty"`
	Labels              []string        `json:"labels,omitempty"`
}

func (o Objective) achievables() []achiever {
	achievables := make([]achiever, len(o.KeyResults))
	for i, v := range o.KeyResults {
		achievables[i] = v
	}
	return achievables
}

func (o Objective) IsAchieved() (bool, error) {
	return isAchieved(o.achievables(), o.CompletionThreshold)
}

func (o Objective) CurrentScore() (float32, error) {
	return score(o.achievables())
}

type KeyResult struct {
	ID         string `json:"id,omitempty"`
	Definition string `json:"definition,omitempty"`
	IsComplete bool   `json:"is_complete,omitempty"`
}

func (k KeyResult) IsAchieved() (bool, error) {
	return k.IsComplete, nil
}

type InitiativeType int

const (
	UnknownInitiativeType InitiativeType = iota
	Book
	Video
)

func (it InitiativeType) String() string {
	return [...]string{"Unknown", "Book"}[it]
}

type Initiative struct {
	ID          string
	Name        string
	Description string
	URL         string
	Type        InitiativeType
}

type validator interface {
	IsValid() (bool, error)
}

type achiever interface {
	IsAchieved() (bool, error)
}

func isAchieved(obj []achiever, threshold float32) (bool, error) {
	score, err := score(obj)

	if err != nil {
		return false, err
	}

	return score >= threshold, nil
}

func score(obj []achiever) (float32, error) {
	numberOfAchievables := len(obj)

	if numberOfAchievables == 0 {
		return 0, nil
	}

	numberAchieved := 0

	for _, i := range obj {
		if ok, _ := i.IsAchieved(); ok {
			numberAchieved++
		}
	}

	return (float32(numberAchieved) / float32(numberOfAchievables)), nil
}
